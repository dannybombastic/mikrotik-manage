# jan/02/1970 01:05:19 by RouterOS 6.33
# software id = 56NZ-W6K4
#
##
# LIMPIA CFG DE LA ANTENA solo ejecutar una sola vez
/system reset-configuration no-defaults=yes skip-backup=yes
######### empezamos carga de cfg, modificar cpeXXXX y passxxxx
/interface ethernet
set [ find default-name=ether1 ] comment="cpeXXXX"
/ip neighbor discovery
set ether1 comment="cpeXXXX"
/interface wireless security-profiles
set [ find default=yes ] supplicant-identity=MikroTik
add authentication-types=wpa2-psk management-protection=allowed mode=\
    dynamic-keys name=MW-Profile supplicant-identity="" wpa2-pre-shared-key=\
    d9sdh30g8dn248d
/interface wireless
set [ find default-name=wlan1 ] ampdu-priorities=0,1,2,3,4,5,6,7 band=\
    5ghz-a/n channel-width=20/40mhz-Ce disabled=no frequency=5770 \
    frequency-mode=superchannel ht-supported-mcs="mcs-0,mcs-1,mcs-2,mcs-3,mcs-\
    4,mcs-5,mcs-6,mcs-7,mcs-16,mcs-17,mcs-18,mcs-19,mcs-20,mcs-21,mcs-22,mcs-2\
    3" nv2-preshared-key=d9sdh30g8dn248d nv2-security=enabled radio-name=\
    cpeXXXX scan-list=4900-6100 security-profile=MW-Profile ssid=\
    CR_Sector supported-rates-a/g=6Mbps,9Mbps,12Mbps,18Mbps,24Mbps \
    wmm-support=enabled
/interface pppoe-client
add add-default-route=yes default-route-distance=1 disabled=no interface=\
    wlan1 max-mru=1480 max-mtu=1480 name=pppoe-out1 password=passxxxx \
    use-peer-dns=yes user=cpeXXXX
/interface wireless nstreme
set wlan1 enable-nstreme=yes
/ip neighbor discovery
set wlan1 discover=no
/ip ipsec proposal
set [ find default=yes ] enc-algorithms=3des
/ip pool
add name=dhcp_pool1 ranges=192.168.100.2-192.168.100.254
/ip dhcp-server
add address-pool=dhcp_pool1 disabled=no interface=ether1 lease-time=1h name=\
    dhcp1
/snmp community
set [ find default=yes ] name=mwdata
/system logging action
set 0 memory-lines=100
/ip address
add address=192.168.100.1/24 comment="eth1 configuration" interface=ether1 \
    network=192.168.100.0
/ip dhcp-server network
add address=192.168.100.0/24 dns-server=192.168.100.1 gateway=192.168.100.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.100.1 name=router
/ip firewall filter
add chain=input comment="default configuration" protocol=icmp
add chain=input comment="default configuration" dst-port=8291 protocol=tcp
add chain=input comment="default configuration" connection-state=established
add chain=input comment="default configuration" connection-state=related
# pppoe-out1 not ready
add action=drop chain=input comment="default configuration" in-interface=\
    pppoe-out1
/ip firewall nat
# pppoe-out1 not ready
add action=masquerade chain=srcnat comment="Masquerade local net to internet" \
    out-interface=pppoe-out1 to-addresses=0.0.0.0
# pppoe-out1 not ready
add action=dst-nat chain=dstnat comment=\
    "Redirect all ports to 192.168.100.254" dst-port=!8291 in-interface=\
    pppoe-out1 protocol=tcp to-addresses=192.168.100.254 to-ports=0-65535
/ip service
set telnet disabled=yes
set ftp disabled=yes
set www disabled=yes
set ssh disabled=yes
set api disabled=yes
set api-ssl disabled=yes
/snmp
set contact=Conred enabled=yes location=Conred_Cliente
/system clock
set time-zone-autodetect=no time-zone-name=Europe/Madrid
/system identity
set name=cpeXXXX
/system leds
set 0 interface=wlan1
/system logging
add action=disk topics=critical
add action=disk topics=error
add action=disk topics=info
add action=disk topics=warning
/system ntp client
set enabled=yes primary-ntp=10.190.10.1 secondary-ntp=10.190.12.1
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=ether1
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes
add interface=ether1
/tool romon port
set [ find default=yes ] cost=100 forbid=no interface=all secrets=""
user set password=1add2020! admin
