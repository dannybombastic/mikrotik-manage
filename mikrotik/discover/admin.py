from django.contrib import admin

# Register your models here.
from .models import Device


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):

    list_per_page = 200

    ordering = ('user',)

    list_display = (
        'ipv4',
        'macaddress',
        'model',
        'interface',
        'user'

    )

    search_fields = ['user__username', 'macaddress', 'create', 'model']
    list_filter = (
        'macaddress',
        'user',
        'create',
        'model'

    )
