from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from ...models import Device
from os import listdir, sys, getenv, path
from datetime import datetime, timedelta
from django.db import connection
from django.utils import timezone
import json
import re
import pexpect
import subprocess
import time
import logging
 


class Loger:

    filename = None
    logger = None
    ft = '%(filename)s | %(levelname)s | %(asctime)s |--------------------|%(message)s'

    def __init__(self, filename, name):
        self.filename = filename
        logging.basicConfig(
            filename=self.filename,
            filemode='a',
            format=self.ft,
            datefmt='%m/%d/%Y %I:%M:%S %p',
        )
        self.logger = logging.getLogger(name)
        self.log_info('Startring proccess')
    

    def log_debug(self, msg):
        self.logger.setLevel(logging.DEBUG)
        self.logger.debug(msg)

    def log_info(self, msg):
        self.logger.setLevel(logging.INFO)
        self.logger.info(msg)

    def log_warning(self, msg):
        self.logger.setLevel(logging.WARNING)
        self.logger.warning(msg)

    def console_info(self, msg):
        msg_console = self.bcolors.OKGREEN + \
            '| {0: >50} |'.format(msg) + self.bcolors.ENDC
        print(msg_console)
        self.log_info(msg)

    def console_success(self, msg):
        msg_console = self.bcolors.OKBLUE + \
            '| {0: >50} |'.format(msg) + self.bcolors.ENDC
        print(msg_console)
        self.log_info(msg)

    def console_warning(self, msg):
        msg_console = self.bcolors.WARNING + \
            '|{0: >50}|'.format(msg) + self.bcolors.ENDC
        print(msg_console)
        self.log_warning(msg)

    class bcolors:
        HEADER = '\033[95m'
        OKBLUE = '\033[94m'
        OKGREEN = '\033[92m'
        WARNING = '\033[93m'
        FAIL = '\033[91m'
        ENDC = '\033[0m'
        BOLD = '\033[1m'
        UNDERLINE = '\033[4m'


log = Loger('./mikrotik.log', 'Mikrotik')


class Command(BaseCommand):

    USER = None
    CPE = None
    PASSWORD = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.USER = getenv('LOGNAME')
        self.routers = []

    def add_arguments(self, parser):
        try:
            parser.add_argument(
                '-u', '--update', type=str, help='Elige el nodo que quieres actualizar ej 192.168.0.1', )
            parser.add_argument(
                '-l', '--list', type=int, help='Escanea la red en busca de MACs',  default=10)

        except AttributeError as e:
            self.stderr.write(str(e))

    def handle(self, *args, **options):
        try:
            if options['update']:
                self.stderr.write('Actualizando')
                value = options['update']
                print(f'Actualizando {value}')
                self.get_conection()

            if options['list']:
                self.stderr.write('Listando')
                value = options['list']
                print(f'Listando {value}')
                self.scan(value)

        except AttributeError as e:
            self.stderr.write(str(e))

    def scan(self, timeout=10):
        """ scan for device info """
        mac_list = []
        list_mikrotik = subprocess.Popen(
            ["mactelnet", "-l", f"-t {timeout}"], stdout=subprocess.PIPE)

        while list_mikrotik.stdout.readable():
            line = list_mikrotik.stdout.readline()
            if not line:
                break
            ###############
            # header info off
            if 'MAC' not in line.strip().decode('ascii'):
                mac_list.append(line.strip().decode('ascii'))

        mac_list = set(mac_list)
        i = 0
        for device in mac_list:
            i += 1
            model = re.search(r'\((.*())\)', device)
            interface = re.search(r'hours (.*)', device)
            split_chain = device.split(' ')
            ipv4 = split_chain[0]
            mac = re.compile(r'(?:[0-9a-fA-F]:?){12}').search(device)

            device_model = Device()
            if mac:
                device_model.model = model.group(1)
                device_model.macaddress = device[mac.start():mac.end()]
                device_model.ipv4 = ipv4
                device_model.interface = interface.group(1)
                device_model.user = self.create_user()
                device_model.save()
                self.format_device_list(device_model, i)

    def format_device_list(self, device, index):
        template = f"""  {log.bcolors.OKGREEN}
option:{log.bcolors.OKBLUE} ({index}) {log.bcolors.ENDC} {log.bcolors.OKGREEN}
Ipv4: {device.ipv4}
MacAddress: {device.macaddress} 
model: {device.model}
Interface: {device.interface}
User: {device.user}                       
                    """
        print(template)

    def create_user(self):
        user = ''
        if not User.objects.filter(username=self.USER).exists():
            user = User.objects.create(username=self.USER)
        else:
            user = User.objects.get(username=self.USER)
        return user

    def automate_login(self):
        list_mikrotik = subprocess.run(["mactelnet", "-l", "-t 10"])

        # child = pexpect.spawn('mactelnet',['D4:CA:6D:C1:99:1A'])

        # child.expect('Login:')
        # child.sendline('admin\r')
        # child.expect('Password:')

        # child.sendline("\r")
        # i = child.expect('[Conneting to .*]')
        # print(i)

        # child.delaybeforesend = 50
        # child.sendline('/ip address print')
        # child.expect('[Mikrotik]')
        # child.sendline('/interface ethernet\r')
        # child.expect('[ethernet ]')
        # child.sendline('set [ find default-name=ether1 ] comment="cpeXXXX_VIDEOS_chaptin"\r')
        # child.delaybeforesend = 10
        # child.sendline('/interface wireless\r')
        # child.expect('[wireless ]')
        # child.sendline('set [ find default-name=wlan1 ] comment="cpeXXXX_VIDEOS_chapatin"\r')
        # child.expect('[wireless]')

        # child.delaybeforesend = 45
        # child.sendline('/quit\r\n')
        # child.expect(pexpect.EOF)
        # child.wait()
        # child.interact()

        mac_telnet = pexpect.spawn('mactelnet', ['D4:CA:6D:C1:99:1A'])
        mac_telnet.expect('Login:')
        mac_telnet.sendline('admin\r')
        mac_telnet.expect('Password:')
        mac_telnet.sendline("\r")
        try:
            index = mac_telnet.expect(['done', pexpect.TIMEOUT], timeout=60)
        except pexpect.EOF:
            pass
        print(f'{index}')
        if index == 1:
            print('TIMEOUT in ps command...')
            print(str(mac_telnet))
            time.sleep(13)
        if index == 0:
            print('conectando...')
            mac_telnet.delaybeforesend = 50

            mac_telnet.sendline(
                '/interface wireless; set [ find default-name=wlan1 ] comment="cpeXXXX_VIDEOS_chapa"; /interface ethernet; set [ find default-name=ether1 ] comment="cpeXXXX_VIDEOS_chapo"\r')
            mac_telnet.expect('[Mikrotik ]')
            mac_telnet.delaybeforesend = 10
            mac_telnet.sendline(
                '/interface wireless; set [ find default-name=wlan1 ] comment="cpeXXXX_VIDEOS_chapa"; '
                + '/interface ethernet; set [ find default-name=ether1 ] comment="cpeXXXX_VIDEOS_chapo"\r')
            mac_telnet.expect('[Mikrotik ]')
            mac_telnet.delaybeforesend = 10
            mac_telnet.sendline(
                '/interface wireless; set [ find default-name=wlan1 ] comment="cpeXXXX_VIDEOS_chapa"; '
                + '/interface ethernet; set [ find default-name=ether1 ] comment="cpeXXXX_VIDEOS_chapo"\r')
            mac_telnet.expect('[Mikrotik ]')
# while True:
#         ps = pexpect.spawn ('ps')
#         time.sleep (1)
#         index = ps.expect (['/usr/bin/ssh', pexpect.EOF, pexpect.TIMEOUT])
#         if index == 2:
#             print('TIMEOUT in ps command...')
#             print(str(ps))
#             time.sleep (13)
#         if index == 1:
#             print(time.asctime(), end=' ')
#             print('restarting tunnel')
#             start_tunnel ()
#             time.sleep (11)
#             print('tunnel OK')
#         else:
#             # print 'tunnel OK'
#             time.sleep (7)
