from django.db import models
from django.contrib.auth.models import User


class Device(models.Model):
    user = models.ForeignKey(User, blank=True,
                             null=True, on_delete=models.CASCADE)
    model = models.CharField(max_length=255,
                             blank=True)

    macaddress = models.CharField(max_length=220,
                                  blank=True, null=True)
    interface = models.CharField(max_length=220,
                                 blank=True, null=True)
    ipv4 = models.CharField(max_length=220,
                            blank=True, null=True)
    cpe = models.CharField(max_length=220,
                            blank=True, null=True)
    cpe_pass = models.CharField(max_length=220,
                            blank=True, null=True)

    create = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'Model : {self.model} , Version {self.version}, Mac-Address: {self.macaddress} Interface {self.interface}, Ipv4 {self.ipv4}'
